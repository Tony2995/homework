'use strict'

function format(formatString, infoObj) {

    var str = formatString.replace(/\{%([a-z A-Z]+)\}/g, total);

    function total(str) {
        var gans = str.match(/\{%([a-z A-Z]+)\}/);
        var tot = gans[1];
        for (var prop in infoObj) {
            if (tot == prop) {
                tot = infoObj[prop]
            }
        }

        return tot; // {%x} => x
    }

    return str;

}

var Obj = {
    x: '10',
    val: '20',
    z: 'zxcv'
    
};

var mes = prompt('enter', '');
var a = format(mes, Obj);
alert(a);


var success = true;
success = success && (format('{%x}', {x: '10'}) == '10');
success = success && (format('qwer{%x}asdf', {x: '10'}) == 'qwer10asdf');
//success = success && (format('zxcvz{%x}asdf{%val}qwer', {x: '10', val: '20', z: 'zxcv'}));
alert(success);
