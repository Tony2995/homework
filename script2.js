/*
Данная функция принимает значения вида: '{%x}','{%z}', {%val}, {%x}qwer{%val} и т.д..

Функция вытаскивает значения содержащееся в фигурных скобках {%value}. Это значение равно 
ключу в объекте, фу-я возвращает значение  ключа. Значение вводит пользователь. 
Хранятся они в объекте. 

То есть, пользователь вводит строку произвольных символов, которая может содержать ключевое 
значение {%value}. Функция игнорирует строку. 

Пример: 'qwer{%x}asdf{%val}' => 'qwer10asdf20'.


*/

// далее, построчное объяснение функции

'use strict' // используем строгий режим стандарта ECMA Script 5

function format(formatString, infoObj) { // объявляем функцию, которая принимает строку и объект

    var str = formatString.replace(/\{%([a-z A-Z]+)\}/g, total); // переменная "str" хранит в себе 
    // "отреплэйсиную" произвольную строку. Регулярное выражение ищет совпадения вида "{%value}"
    // и, далее используется функция 'total', возвращающая значение. 
    //Пример: '{%x}' => x => 10(из данного объекта)

    function total(str) { // функция принимает строку 'str'
        var tot = arguments[1]; 
        /* здесь, мы берем один из аргументов, которые передаются в данную функцию
        в частности, нас интересует value, т.е {%value}. Таким образом мы и вытаскиваем его.
        */

        if(tot in infoObj){
            tot = infoObj[tot]
        }

        /* Условие проверяет, если есть совпадения в объекте с одним из его ключей.
            Совпадение есть, возвращаем значение ключа.   
        */


      
        return tot; // Возвращаем искомое значение
    }


    return str; // Возвращаем исходную строку с измененными значениями 

}

var Obj = { // объект хранит в себе возможные значения
    x: '10',
    val: '20',
    z: 'zxcv'
    /*
        Данные значения - для примера. Можно изменить, в зависимости от задачи.

    */
    
};

var mes = prompt('enter', ''); // принимаем строку от пользователя
var a = format(mes, Obj); // cохраняем функцию в переменную. Передаем функции 'format' значения строки и объекта
alert(a); // смотрим, что вышло
/*
    Блок ниже - блок проеврки. Написан Антоном. Тест. Проверяет совпадения. 

*/

var success = true; // присваиваем истину. Далее, если удовлитворяет услвою => true.
success = success && (format('{%x}', {x: '10'}) == '10');// возвращаем из исходной строки {%x} => 10
success = success && (format('qwer{%x}asdf', {x: '10'}) == 'qwer10asdf'); // строку и значение
success = success && (format('zxcvz{%x}asdf{%val}qwer', {x: '10', val: '20', z: 'zxcv'}) == 'zxcvz10asdf20qwer');
//третяя строка теста проверяет значения и строку
alert(success); // успех или нет